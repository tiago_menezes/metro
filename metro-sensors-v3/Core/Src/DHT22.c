/**
 ******************************************************************************
 * @file    DHT22.c
 * @author  Peterson Aguiar
 * @brief   DHT22 device layer
 * @brief   Version 2019.8.1
 * @brief   Creation Date 23/08/2019
 ******************************************************************************
 */

#include "DHT22.h"
#include "string.h"
#include "macros.h"

uint32_t Read_High_Pulse_Time(uint8_t ID, uint32_t *time);
uint32_t Read_Low_Pulse_Time(uint8_t ID, uint32_t *time);
void Wait_Microseconds(uint32_t microseconds_value);
void Init_Pin(uint8_t ID);
void Set_Data_Pin_Output(uint8_t ID);
void Set_Data_Pin_Input(uint8_t ID);
uint8_t Get_Data_Pin_Value(uint8_t ID);
void Set_Data_Pin_High(uint8_t ID);
void Set_Data_Pin_Low(uint8_t ID);

DEV_DHT22_Param_t Sensor_Devices[DEV_DHT22_MAX_ID_LIMIT];
static uint8_t DEV_DHT22_IsIdInit[DEV_DHT22_MAX_ID_LIMIT];

GPIO_Parameters_t  GPIO_Configuration;

ReturnCode_t DEV_DHT22_IntHwInit(uint8_t ID)
{
  ReturnCode_t RetCode = ANSWERED_REQUEST;
  if( ID >= DEV_DHT22_MAX_ID_LIMIT )
    RetCode = ERR_PARAM_ID;
  if(DEV_DHT22_IsIdInit[ID] != 0 )
    RetCode = ERR_ENABLED;
  else
  {
//    __GPIOB_CLK_ENABLE();
//    __TIM2_CLK_ENABLE();
//    TimerDHT22.Instance = TIM2;
//    TimerDHT22.Init.Prescaler = 16;
//    TimerDHT22.Init.CounterMode = TIM_COUNTERMODE_UP;
//    TimerDHT22.Init.Period = 0xFFFFFFFF;
//    TimerDHT22.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//    HAL_TIM_Base_Init(&TimerDHT22);
//    HAL_TIM_Base_Start(&TimerDHT22);
  }

  return RetCode;
}

ReturnCode_t DEV_DHT22_ExtDevConfig(uint8_t ID, DEV_DHT22_Param_t Parameter)
{
  ReturnCode_t RetCode = ANSWERED_REQUEST;
  if( ID >= DEV_DHT22_MAX_ID_LIMIT )
  {
    RetCode = ERR_PARAM_ID;
  }
  else
  {
    memcpy(&Sensor_Devices[ID], &Parameter, sizeof(DEV_DHT22_Param_t));
    Init_Pin(ID);
  }
  return RetCode;
}

ReturnCode_t DEV_DHT22_Sensor_Read(uint8_t ID, int16_t *Temperature, uint16_t *Humidity)
{
  ReturnCode_t RetCode = ANSWERED_REQUEST;
  uint32_t High_Time, Low_Time;
  uint8_t BitCounter;
  uint16_t Humi, Checksum_Calc;
  int16_t Temp;
  uint8_t Checksum;
  uint64_t DataBits;
  if( ID <= DEV_DHT22_MAX_ID_LIMIT )
  {
    Set_Data_Pin_Output(ID);
    Set_Data_Pin_Low(ID);
    Wait_Microseconds(1000);
    Set_Data_Pin_Input(ID);
    if(Read_High_Pulse_Time(ID, &High_Time))  // Initial Pulse (aprox 20 ~40us)
      return ERR_OVERFLOW;
    if(Read_Low_Pulse_Time(ID, &Low_Time))    // Start bit Low part (~80us)
      return ERR_OVERFLOW;
    if(Read_High_Pulse_Time(ID, &High_Time))  // Start bit High part (~80us)
      return ERR_OVERFLOW;
    if((Low_Time + High_Time) > 170) // Start bit error
      return ERR_OVERFLOW;

    DataBits = 0;
    for (BitCounter = 0; BitCounter < 40; ++BitCounter)
    {
      if(Read_Low_Pulse_Time(ID, &Low_Time))
        return ERR_OVERFLOW;
      if(Read_High_Pulse_Time(ID, &High_Time))
        return ERR_OVERFLOW;
      DataBits <<= 1;
      if((High_Time >= MIN_TIME_HIGH_LEVEL) && (High_Time <= MAX_TIME_HIGH_LEVEL))
        DataBits |= 1;
    }

    Checksum = DataBits & 0xFF;

    Checksum_Calc = 0;
    for (BitCounter = 1; BitCounter < 5; ++BitCounter)
      Checksum_Calc += (DataBits >> 8 * BitCounter) & 0xFF;

    Checksum_Calc &= 0xFF;
    if(Checksum != Checksum_Calc)
      return ERR_CRC;
    Temp = (DataBits >> 8) & 0x7FFF;

    if(((DataBits >> 23) & 0x1) == 1)
    {
      Temp = -Temp;
    }
    Humi = (DataBits >> 24) & 0xFFFF;

    *Temperature = Temp;
    *Humidity = Humi;
  }
  else
  {
    RetCode = ERR_PARAM_ID;
  }
  return RetCode;
}

void Set_Data_Pin_High(uint8_t ID)
{
  GPIO_SetOutput(Sensor_Devices[ID].GPIO_Id);
}

void Set_Data_Pin_Low(uint8_t ID)
{
  GPIO_ClearOutput(Sensor_Devices[ID].GPIO_Id);
}

uint8_t Get_Data_Pin_Value(uint8_t ID)
{
  uint8_t GPIO_Input_Value;
  GPIO_ReadInput(Sensor_Devices[ID].GPIO_Id, &GPIO_Input_Value);
  return GPIO_Input_Value;
}

void Init_Pin(uint8_t ID)
{
  GPIO_Configuration.GPIO_Direction = GPIO_MODE_IN_PULL_UP;
  GPIO_Configuration.GPIO_Pin   = Sensor_Devices[ID].GPIO_Pin;
  GPIO_Configuration.GPIO_Port  = Sensor_Devices[ID].GPIOx;
  GPIO_Init(Sensor_Devices[ID].GPIO_Id, GPIO_Configuration);
}

void Set_Data_Pin_Output(uint8_t ID)
{
  GPIO_Configuration.GPIO_Direction = GPIO_MODE_OUT_PP;
  GPIO_Configuration.GPIO_Pin   = Sensor_Devices[ID].GPIO_Pin;
  GPIO_Configuration.GPIO_Port  = Sensor_Devices[ID].GPIOx;
  GPIO_Reconfigure(Sensor_Devices[ID].GPIO_Id, GPIO_Configuration);
}

void Set_Data_Pin_Input(uint8_t ID)
{
  GPIO_Configuration.GPIO_Direction = GPIO_MODE_IN_PULL_UP;
  GPIO_Configuration.GPIO_Pin   = Sensor_Devices[ID].GPIO_Pin;
  GPIO_Configuration.GPIO_Port  = Sensor_Devices[ID].GPIOx;
  GPIO_Reconfigure(Sensor_Devices[ID].GPIO_Id, GPIO_Configuration);
}

void Wait_Microseconds(uint32_t microseconds_value)
{
  uint32_t TTime;

  TTime = 0;
  START_EXECUTION_TIME_MEASUREMENT();
  while(TTime < microseconds_value)
  {
    TTime = GET_EXEC_TIME_US();
  }
}

uint32_t Read_Low_Pulse_Time(uint8_t ID, uint32_t *time)
{
  uint32_t TTime;
  START_EXECUTION_TIME_MEASUREMENT();
  do{
    TTime = GET_EXEC_TIME_US();
    if(TTime > 100)
      return 1;
  }while(!Get_Data_Pin_Value(ID));

  *time = TTime;
  return 0;
}

uint32_t Read_High_Pulse_Time(uint8_t ID, uint32_t *time)
{
  uint32_t TTime;
  START_EXECUTION_TIME_MEASUREMENT();
  do{
    TTime = GET_EXEC_TIME_US();
    if(TTime > 100)
      return 1;
  }while(Get_Data_Pin_Value(ID));
  *time = TTime;
  return 0;
}
