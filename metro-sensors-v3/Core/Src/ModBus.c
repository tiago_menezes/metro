/*
 * ModBus.c
 *
 *  Created on: 8 de fev de 2021
 *      Author: Samuel
 */


#include "ModBus.h"
#include "stddef.h"
#include "string.h"


void ModBus_getRegister(ModBusMaster_t * master, uint8_t item)
{
	static uint8_t size;
	size = 0;

	if(item < master->objQty)
	{
		master->BUFFER[size++] = master->ModBusRegisters[item].slaveAddress;
		master->BUFFER[size++] = 0x03;
		master->BUFFER[size++] = ((master->ModBusRegisters[item].regAddress >> 8) & 0x0FF);
		master->BUFFER[size++] = ( master->ModBusRegisters[item].regAddress & 0x0FF);
		master->BUFFER[size++] = ((master->ModBusRegisters[item].Length >> 8) & 0x0FF);
		master->BUFFER[size++] = ( master->ModBusRegisters[item].Length & 0x0FF);
		size = GenCRC16(master->BUFFER, size);
		master->BUFFER[size + 1] = 0;
		master->item = item;
		master->TransmitFunc(master->BUFFER, size);
	}
	else
	{
		// ERROR
	}
}

void modBusMasterInput(ModBusMaster_t * master, uint8_t * data, uint8_t size)
{
	if(!CheckCRC16(data, size))
		return;
	if (data[1] == 0x03)
	{
		int16_t * reg = master->ModBusRegisters[master->item].Register;

		for (uint8_t i = 0; i < master->ModBusRegisters[master->item].Length; i++)
		{
			*reg++ = (((uint16_t) (data[3 + (i << 1)]) << 8) + data[4 + (i << 1)]);
		}
	}
}



void ModBus_setRegisters(ModBusMaster_t * master, uint8_t item)
{

	static uint8_t size;
	size = 0;

	if(item > master->objQty)
	{
		int16_t * reg = master->ModBusRegisters[master->item].Register;
		memset(master->BUFFER, 0, 32);
		master->BUFFER[size++] = master->ModBusRegisters[item].slaveAddress;
		master->BUFFER[size++] = 0x03;
		master->BUFFER[size++] = ((master->ModBusRegisters[item].regAddress >> 8) & 0x0FF);
		master->BUFFER[size++] = ( master->ModBusRegisters[item].regAddress & 0x0FF);
		master->BUFFER[size++] = ((master->ModBusRegisters[item].Length >> 8) & 0x0FF);
		master->BUFFER[size++] = ( master->ModBusRegisters[item].Length & 0x0FF);
		master->BUFFER[size++] =   master->ModBusRegisters[item].Length * 2;

		for (uint8_t i = 0; i < master->ModBusRegisters[item].Length; i++)
		{
			master->BUFFER[size++] =  (*reg >> 8) & 0x0FF;
			master->BUFFER[size++] =  (*reg++     & 0x0FF);
		}

		size = GenCRC16(master->BUFFER, size);
		master->item = -1;
		master->TransmitFunc(master->BUFFER, size);
	}
	else
	{
		// ERROR
	}
}


void ModBusSlaveInput(ModBusSlave_t * slave, uint8_t * data, uint8_t len)
{
	if(!(data[0] == slave->slaveAddress))
		return;

	if(!CheckCRC16(data, len))
		return; // ERRO CRC

	static uint8_t size;

	int16_t * reg = NULL;
	uint16_t regAddr = ((data[2] << 8) + data[3]);


	for(uint8_t i = 0; i < slave->objQty; i++)
	{
		if((regAddr == slave->ModBusRegisters[i].regAddress) && (((data[4] << 8) + data[5]) <= slave->ModBusRegisters[i].Length))
		{
			reg = slave->ModBusRegisters[i].Register;
			break;
		}
	}

	if(reg == NULL) return; // error address not exists

	switch (data[1])
	{
	case 0x03:
	{
		size = 0;
		memset(slave->BUFFER, 0, 32);
		slave->BUFFER[size++] = slave->slaveAddress;
		slave->BUFFER[size++] = 0x03;
		slave->BUFFER[size++] = ((data[4] << 8) + data[5]) * 2;

		for (uint8_t i = 0; i < ((data[4] << 8) + data[5]); i++)
		{
			slave->BUFFER[size++] = ((reg[i]) >> 8) & 0x0FF;
			slave->BUFFER[size++] =  (reg[i])  & 0x0FF;
		}

		size = GenCRC16(slave->BUFFER, size);
		if(slave->TransmitFunc != NULL) slave->TransmitFunc(slave->BUFFER, size);
	}
	break;

	case 0x10:
	{
//		uint16_t address 	= (data[2] << 8) + data[3];
//		uint16_t count 		= (data[4] << 8) + data[5];
//
//		for (uint16_t i = 0; i < count; i++)
//		{
//			frame->Registers[address + i] = ((uint16_t)(*(data + 7 + i * 2)) << 8) + (uint16_t)(*(data + 8 + i * 2));
//		}
//		frame->m_size = 0;
//		frame->m_data[frame->m_size++] = frame->slave_address;
//		frame->m_data[frame->m_size++] = WRITE_MULTI_REGISTER;
//		frame->m_data[frame->m_size++] = (address >> 8) & 0x0FF;
//		frame->m_data[frame->m_size++] = address & 0x0FF;
//		frame->m_data[frame->m_size++] = (count >> 8) & 0x0FF;
//		frame->m_data[frame->m_size++] = count & 0x0FF;
//		frame->m_size = GenCRC16(frame->m_data, frame->m_size);
//		TransmitFunc(frame->m_data, frame->m_size);

		break;
	}
	default:
		break;
	}
}
