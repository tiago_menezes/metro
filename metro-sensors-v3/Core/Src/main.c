/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stdio.h"
#include "math.h"
#include "ModBus.h"
#include "DHT22.h"
#include "bmp280.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TIM1_CLOCK 		168e6
#define TIM1_PRESCALAR 	1680
#define TIM1_REF_CLOCK	(TIM1_CLOCK/TIM1_PRESCALAR)
#define TIM1_US_FACTOR	(1e6/TIM1_REF_CLOCK) // Conversion factor to us

#define TIM2_CLOCK		(TIM1_CLOCK/2)
#define TIM2_PRESCALAR	8400
#define TIM2_REF_CLOCK	(TIM2_CLOCK/TIM2_PRESCALAR)
#define TIM2_US_FACTOR	(1e6/TIM2_REF_CLOCK) // Conversion factor to us
#define TIM2_US_PERIOD	((htim2.Init.Period + 1) * TIM2_US_FACTOR)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*Modbus*/
uint8_t modbusMasterData[16];
uint8_t hasPassedThirtySeconds = 0;
void transmitDataEndPoint(uint8_t *data , uint8_t size)
{
	HAL_UART_Transmit_IT(&huart2, data, size);
}

/*Intermediate variables*/
uint32_t windDirectionAdcValue = 0, anemometerIcValue = 0,  anemometerFrequency = 0;
uint32_t dustHighCapture10 = 0, dustLowCapture10 = 0, dustCaptureDiff10 = 0, dustTotalLowTime10 = 0;
uint32_t dustHighCapture25 = 0, dustLowCapture25 = 0, dustCaptureDiff25 = 0, dustTotalLowTime25 = 0;
float dustLowRatio10 = 0, dustLowRatio25 = 0, dustOut10 = 0, dustOut25 = 0;
float bmeTemperature = 0, bmeHumidity = 0, bmePressure = 0;

/*Final variables*/
int16_t temperature = 0;
uint16_t  humidity = 0, pressure = 0, windDirection = 0,  anemometerVelocity = 0, pm10 = 0, pm25 = 0;

/*ModBus register; should match on toradex*/
ModBusSlaveRegisters_t MBS_ENDPOINT_REGS [] =
{
		MBS_ADD_REGISTER(temperature, 30, 1),
		MBS_ADD_REGISTER(humidity, 31, 1),
		MBS_ADD_REGISTER(pressure, 32, 1),
		MBS_ADD_REGISTER(windDirection, 33, 1),
		MBS_ADD_REGISTER(anemometerVelocity, 34, 1),
		MBS_ADD_REGISTER(pm10, 35, 1),
		MBS_ADD_REGISTER(pm25, 36, 1),
};

MODBUS_SLAVE(END_POINT, MBS_ENDPOINT_REGS, 4, transmitDataEndPoint);

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
		//Mock values, delete later
	temperature = 245; humidity = 809, pressure = 10110, windDirection = 120;  anemometerVelocity = 50; pm10 = 1000, pm25 = 800;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  //Temperature and Humidity on GPIO -> PB10
  DEV_DHT22_Param_t Temperature_Sensor_Parameters = {.GPIOx = mTEMP_GPIO_Port, .GPIO_Pin = mTEMP_Pin, .GPIO_Id = DEV_DHT22_GPIO_ID};
  DEV_DHT22_ExtDevConfig(DEV_DHT22_GPIO_ID, Temperature_Sensor_Parameters);

  //Pressure on I2C1 -> PB6 (SCL) / PB7 (SDA)
  BMP280_HandleTypedef bmp280;
  bmp280_init_default_params(&bmp280.params);
  bmp280.addr = BMP280_I2C_ADDRESS_0;
  bmp280.i2c = &hi2c1;
  bmp280_init(&bmp280, &bmp280.params);

  //Wind direction on ADC1 IN7 -> PA7
  HAL_ADC_Start_IT(&hadc1);

  //Anemometer on TIM1 CH3 -> PE13
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_3);

  //Dust sensor DUST-1_0 on TIM1 CH1 -> PE9 and DUST-2_5 on TIM1 CH2 -> PE11
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_1); // PM10
  HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_2); // PM25

  //MODBUS on UART -> PA2 (TX) / PA3 (RX)
  HAL_UARTEx_ReceiveToIdle_IT(&huart2, modbusMasterData, 16);

  //30 seconds interrupt TIM2 used for all measurements
  HAL_TIM_Base_Start_IT(&htim2);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if(hasPassedThirtySeconds) //MODBUS requests every 40s, doesn't need to refresh values every loop
	  {
		  /* TEMP & HUMID */
		  DEV_DHT22_Sensor_Read(DEV_DHT22_GPIO_ID, &temperature, &humidity); // °C,%; x10 (from sensor)

		  /* PRESSURE */
		  bmp280_read_float(&bmp280, &bmeTemperature, &bmePressure, &bmeHumidity);
		  pressure = (uint32_t)(bmePressure/10.0); // kPa; x100

		  /* WIND */
		  HAL_ADC_Start_IT(&hadc1); // One value has already been stored in AdcValue. Start again to update value for next reading
		  windDirection = (uint16_t) (360.0 * windDirectionAdcValue / 4095.0); // 0-360°; x1

		  /* ANEMOMETER */
		  anemometerVelocity = (uint16_t) (10.0 * (0.0462 * anemometerFrequency + 0.21)); // m/s; x10

		  /* DUST */
		  dustOut10 = 0.1776*pow(dustLowRatio10,3) - 0.24*pow(dustLowRatio10,2) + 94.003*dustLowRatio10;
		  pm10 = dustOut10 < 0 ? 0 : (uint16_t)dustOut10; // ug/m3; PM10 >= 1.0um particles; x1
		  dustOut25 = 0.1776*pow(dustLowRatio25,3) - 0.24*pow(dustLowRatio25,2) + 94.003*dustLowRatio25;
		  pm25 = (dustOut25 < 0) || (pm10 < dustOut25) ? 0 : (uint16_t)(pm10 - dustOut25); // ug/m3; 2.5um >= PM25 >= 1.0um particles; x1

		  hasPassedThirtySeconds = 0;
	  }

	  HAL_Delay(100);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM1){
		//Particle sensor 1.0 um
		if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
			if (!HAL_GPIO_ReadPin(DUST_1_0_GPIO_Port, DUST_1_0_Pin)) { // if the first value is not captured
				dustHighCapture10 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); // read the first value
			} else {   // if the first is already captured
				dustLowCapture10 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);  // read second value
				if (dustLowCapture10 >= dustHighCapture10) {
					dustCaptureDiff10 = dustLowCapture10-dustHighCapture10;
				} else {
					dustCaptureDiff10 = dustHighCapture10-dustLowCapture10;
				}

				dustTotalLowTime10 += (dustCaptureDiff10*TIM1_US_FACTOR);

				__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter
			}
		}
    //Particle sensor 2.5 um
    else if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2) {
      if (!HAL_GPIO_ReadPin(DUST_2_5_GPIO_Port, DUST_2_5_Pin)) { // if the first value is not captured
				dustHighCapture25 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2); // read the first value
			} else {   // if the first is already captured
				dustLowCapture25 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);  // read second value
				if (dustLowCapture25 >= dustHighCapture25) {
					dustCaptureDiff25 = dustLowCapture25-dustHighCapture25;
				} else {
					dustCaptureDiff25 = dustHighCapture25-dustLowCapture25;
				}

				dustTotalLowTime25 += (dustCaptureDiff25*TIM1_US_FACTOR);

				__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter
			}
    }
		//Anemometer
		else if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3) {
			// Read the IC value
			anemometerIcValue = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
			if (anemometerIcValue != 0)
			{
				anemometerFrequency = TIM1_REF_CLOCK/anemometerIcValue;
				__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter
			}
		}
	}
}

// Counter Overflow ISR Handler
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	if(htim->Instance == TIM2)
	{
		dustLowRatio10 = 100 * dustTotalLowTime10 / TIM2_US_PERIOD; // Also convert to 0-100%
		dustTotalLowTime10 = 0;
		dustLowRatio25 = 100 * dustTotalLowTime25 / TIM2_US_PERIOD; // Also convert to 0-100%
		dustTotalLowTime25 = 0;

		hasPassedThirtySeconds = 1;
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    // Read & Update The ADC Result
	windDirectionAdcValue = HAL_ADC_GetValue(&hadc1);
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size) {
	if(huart->Instance == USART2)
	{
		ModBusSlaveInput(&END_POINT, modbusMasterData, Size);
		HAL_UARTEx_ReceiveToIdle_IT(&huart2, modbusMasterData, 16);
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
