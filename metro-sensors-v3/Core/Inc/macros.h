/* *****************************************************************************
 FILE_NAME:     Macros.h
 DESCRIPTION:   This header file contains macros, designed to meet the needs of
                all projects.
 DESIGNER:      Andre F. N Dainese
                Daniel C. Rebeschini
                Juliano G. P. Varasquim
 CREATION_DATE: 04/08/2015
 *******************************************************************************
 VERSION 0.2:   25/08/2015
                - Introduction of new macros to create variable accessible to
                  the user of the system
 VERSION 0.3:   01/09/2015
                - Introduction of new macros to create variable in array format
                  accessible to the user of the system
 VERSION 0.4:   01/09/2015 - Andre F. N. Dainese
                - Added CONC2BYTE macro
                - Removed all the tab characters.
 VERSION 0.5:   01/09/2015 - Andre F. N. Dainese
                - Bugfixes in the saturate macros.
 VERSION 0.6:   18/04/2017 - Andre F. N. Dainese
                - ADD_MATRIX_VAR and ADD_MATRIX_BIGF now accepts matrices [][]
***************************************************************************** */
#ifndef MACROS_H
#define MACROS_H

#include <string.h>


/* *****************************************************************************
 *        FIRMWARE VERSION
***************************************************************************** */
#define COMMON_MACROS_VER_MAJOR   2020
#define COMMON_MACROS_VER_MINOR   1
#define COMMON_MACROS_VER_PATCH   1
#define COMMON_MACROS_BRANCH_MASTER


/* *****************************************************************************
 DESCRIPTION: Macro to initialize a variable with n positions.
 MACROS:      INITIALIZE_VARIABLE
 DESIGNER:    Andre F. N Dainese
              Daniel C. Rebeschini
 EXAMPLE:
              static uint8 PULSEMONITOR_Saved_ID[NUMBER_VOL_SENSORS] =
              INITIALIZE_VARIABLE(NUMBER_VOL_SENSORS, 0);
***************************************************************************** */
#define INITIALIZE_VARIABLE( SIZE, VALUE )      { [ 0 ... (SIZE - 1) ] = VALUE }
#define REPEATS_N_TIMES_VALUE( N, VALUE )         [ 0 ... (N - 1) ] = VALUE


/* *****************************************************************************
 DESCRIPTION: Macro to check if current branch is supported by application.
 MACROS:      CHECK_BRANCH
 DESIGNER:    Denis Beraldo

 EXAMPLE:     #if CHECK_BRANCH(DATA_LOGGER_BRANCH_MASTER)
              #error "Datalogger branch error!!"
              #endif
***************************************************************************** */
#define CHECK_BRANCH  !defined


/* *****************************************************************************
 DESCRIPTION: Macro to check if current version is supported by application.
 MACROS:      CHECK_VERSION
 DESIGNER:    Denis Beraldo

 EXAMPLE:     #if CHECK_VERSION(DATA_LOGGER_VER, 1,10)
              #error "Datalogger version error!!"
              #endif
***************************************************************************** */
#ifndef __CONCAT
  #define __CONCAT1(x,y)  x ## y
  #define __CONCAT(x,y) __CONCAT1(x,y)
#endif

#define CHECK_VERSION(__PREFIX, __MAJOR, __MINOR, __PATCH)                     \
                    ((__CONCAT(__PREFIX##_, MAJOR) != __MAJOR)  ||             \
                     (__CONCAT(__PREFIX##_, MINOR) != __MINOR)  ||             \
                     (__CONCAT(__PREFIX##_, PATCH) != __PATCH))                \

/* *****************************************************************************

*

* THESE MACROS IS USED TO EXECUTION TIME LIB ANALYZES ON STM32F407

*

*******************************************************************************/

/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */

  #define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))

    /*!< DWT Control register */

  #define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)

    /*!< CYCCNTENA bit in DWT_CONTROL register */

  #define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))

    /*!< DWT Cycle Counter register */

  #define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))

    /*!< DEMCR: Debug Exception and Monitor Control Register */

  #define KIN1_TRCENA_BIT              (1UL<<24)

    /*!< Trace enable bit in DEMCR register */



#define KIN1_InitCycleCounter() \
  KIN1_DEMCR |= KIN1_TRCENA_BIT

  /*!< TRCENA: Enable trace and debug block DEMCR (Debug Exception and Monitor Control Register */



#define KIN1_ResetCycleCounter() \
  KIN1_DWT_CYCCNT = 0
  /*!< Reset cycle counter */



#define KIN1_EnableCycleCounter() \
  KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
  /*!< Enable cycle counter */

#define KIN1_DisableCycleCounter() \
  KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
  /*!< Disable cycle counter */

#define KIN1_GetCycleCounter() \
  KIN1_DWT_CYCCNT

  /*!< Read cycle counter register */

#define START_EXECUTION_TIME_MEASUREMENT()\
  KIN1_InitCycleCounter(); \
  KIN1_ResetCycleCounter(); \
  KIN1_EnableCycleCounter(); \

#define GET_EXEC_TIME_US() \
  0.00595*KIN1_GetCycleCounter();

#endif

