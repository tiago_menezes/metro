/**
 ******************************************************************************
 * @file    DHT22.h
 * @author  Peterson Aguiar
 * @brief   DHT22  (AM2302) device layer
 * @brief   Creation Date 23/08/2019
 ******************************************************************************
 */

#ifndef DEV_DHT22_H_INCLUDED
#define DEV_DHT22_H_INCLUDED


#include "returncode.h"
#include "types.h"
#include "myGPIO.h"
#include "main.h"

#define DEV_DHT22_VER_MAJOR    2019
#define DEV_DHT22_VER_MINOR    12
#define DEV_DHT22_VER_PATCH    1
#define DEV_DHT22_BRANCH_MASTER

#ifndef DEV_DHT22_MAX_ID_LIMIT
#define DEV_DHT22_MAX_ID_LIMIT     1
#endif

#define DEV_DHT22_GPIO_ID (GPIO_Port_t)0

#define MIN_TIME_HIGH_LEVEL 55
#define MAX_TIME_HIGH_LEVEL 100

#define MIN_TIME_LOW_LEVEL 10
#define MAX_TIME_LOW_LEVEL 40


typedef struct{
  GPIO_Port_t GPIOx;
  uint16_t GPIO_Pin;
  uint8_t GPIO_Id;
}DEV_DHT22_Param_t;

ReturnCode_t DEV_DHT22_IntHwInit(uint8_t ID);

ReturnCode_t DEV_DHT22_ExtDevConfig(uint8_t ID, DEV_DHT22_Param_t Parameter);

ReturnCode_t DEV_DHT22_Sensor_Read(uint8_t ID, int16_t *Temperature, uint16_t *Humidity);

#endif

