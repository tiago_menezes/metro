/*
 * ModBus.h
 *
 *  Created on: 7 de abr de 2021
 *      Author: Samuel
 */

#ifndef SRC_MODBUS_MODBUS_H_
#define SRC_MODBUS_MODBUS_H_

#include "stm32f4xx.h"

#define MB_DATA_SIZE 128

typedef void (*mbTransmit)(uint8_t* , uint8_t);

typedef struct
{
	void * 		Register;
	uint8_t 	slaveAddress;
	uint16_t 	regAddress;
	uint16_t 	Length;
}ModBusMasterRegisters_t;

typedef struct
{
	ModBusMasterRegisters_t * ModBusRegisters;
	mbTransmit 			TransmitFunc;
	uint16_t 			objQty;
	uint8_t 			BUFFER[MB_DATA_SIZE];
	int8_t 				item;
}ModBusMaster_t;


#define MBM_ADD_REGISTER(VAR, SLAVE_ADDRESS, REG_ADDRESS, LENGTH) {&VAR, SLAVE_ADDRESS, REG_ADDRESS, LENGTH}

#define MODBUS_MASTER(NAME, REGS, TRANSMITION_FUNC) 		\
ModBusMaster_t NAME = 										\
{ 															\
		REGS,												\
		TRANSMITION_FUNC,									\
		sizeof(REGS)/sizeof(ModBusMasterRegisters_t),		\
		{},													\
		0													\
}

typedef struct
{
	void * 		Register;
	uint16_t 	regAddress;
	uint16_t 	Length;
}ModBusSlaveRegisters_t;

typedef struct
{
	ModBusSlaveRegisters_t * 	ModBusRegisters;
	uint8_t 					slaveAddress;
	mbTransmit 					TransmitFunc;
	uint16_t 					objQty;
	uint8_t 					BUFFER[MB_DATA_SIZE];
}ModBusSlave_t;

#define MBS_ADD_REGISTER(VAR, REG_ADDRESS, LENGTH) {&VAR, REG_ADDRESS, LENGTH}

#define MODBUS_SLAVE(NAME, REGS, ADDR, TRANSMIT_FUNC) 	\
ModBusSlave_t NAME = { 								\
	REGS,											\
	ADDR,											\
	TRANSMIT_FUNC,									\
	sizeof(REGS)/sizeof(ModBusSlaveRegisters_t),	\
	{}												\
}
void ModBus_getRegister(ModBusMaster_t * master, uint8_t item);
void modBusMasterInput(ModBusMaster_t * master, uint8_t * data, uint8_t size);
void ModBus_setRegisters(ModBusMaster_t * master, uint8_t item);
void ModBusSlaveInput(ModBusSlave_t * slave, uint8_t * data, uint8_t len);

static inline uint8_t GenCRC16(uint8_t* buff, uint8_t len)
{
	uint16_t crc = 0xFFFF;
	uint16_t pos = 0;
	uint8_t i = 0;
	uint8_t lo = 0;
	uint8_t hi = 0;

	for (pos = 0; pos < len; pos++)
	{
		crc ^= buff[pos];

		for (i = 8; i != 0; i--)
		{
			if ((crc & 0x0001) != 0)
			{
				crc >>= 1;
				crc ^= 0xA001;
			}
			else
				crc >>= 1;
		}
	}
	lo = crc & 0xFF;
	hi = (crc >> 8) & 0xFF;

	buff[len++] = lo;
	buff[len++] = hi;
	return len;
}

static inline uint8_t CheckCRC16(volatile uint8_t* buff, uint8_t len)
{
	uint16_t crc = 0xFFFF;
	uint16_t pos = 0;
	uint8_t i = 0;
	uint8_t lo = 0;
	uint8_t hi = 0;

	for (pos = 0; pos < len - 2; pos++)
	{
		crc ^= buff[pos];
		for (i = 8; i != 0; i--)
		{
			if ((crc & 0x0001) != 0)
			{
				crc >>= 1;
				crc ^= 0xA001;
			}
			else
				crc >>= 1;
		}
	}
	lo = crc & 0xFF;
	hi = (crc >> 8) & 0xFF;
	if ((buff[len - 2] == lo) && (buff[len - 1] == hi))
	{
		return 1;
	}
	return 0;
}


#endif /* SRC_MODBUS_MODBUS_H_ */
